DESTDIR ?=

SLOG2SDK_VERSION = 1.2.9

prefix = /usr
bindir = $(prefix)/bin
datadir = $(prefix)/share
javadir = $(datadir)/java

INSTALL = install

all: jars

build_jumpshot:
	mkdir -p build_jumpshot
	cp -r src/images/ build_jumpshot/
	cp -r doc/jumpshot-4/html/ build_jumpshot/
	javac -d build_jumpshot/ @sources_jumpshot

build_traceTOslog2:
	mkdir -p build_traceTOslog2
	javac -d build_traceTOslog2/ @sources_traceTOslog2

jumpshot-$(SLOG2SDK_VERSION).jar: build_jumpshot
	cd build_jumpshot/ && jar cfm ../jumpshot-$(SLOG2SDK_VERSION).jar ../MANIFEST.jumpshot *

traceTOslog2-$(SLOG2SDK_VERSION).jar: build_traceTOslog2
	cd build_traceTOslog2/ && jar cf ../traceTOslog2-$(SLOG2SDK_VERSION).jar *

jars: jumpshot-$(SLOG2SDK_VERSION).jar traceTOslog2-$(SLOG2SDK_VERSION).jar
	ln -s jumpshot-$(SLOG2SDK_VERSION).jar jumpshot.jar
	ln -s traceTOslog2-$(SLOG2SDK_VERSION).jar traceTOslog2.jar

install: force
	mkdir -p tmp
	cp bin/jumpshot tmp/
	sed -i 's|JARDIR=.*|JARDIR=$(javadir)|' tmp/jumpshot
	mkdir -p $(DESTDIR)/$(bindir)
	$(INSTALL) -m 755 tmp/jumpshot '$(DESTDIR)/$(bindir)'
	mkdir -p $(DESTDIR)/$(javadir)
	$(INSTALL) -m 644 jumpshot-$(SLOG2SDK_VERSION).jar '$(DESTDIR)/$(javadir)'
	ln -sf jumpshot-$(SLOG2SDK_VERSION).jar $(DESTDIR)/$(javadir)/jumpshot.jar
	$(INSTALL) -m 644 traceTOslog2-$(SLOG2SDK_VERSION).jar '$(DESTDIR)/$(javadir)'
	ln -sf traceTOslog2-$(SLOG2SDK_VERSION).jar $(DESTDIR)/$(javadir)/traceTOslog2.jar

clean:
	rm -rf build_jumpshot/ build_traceTOslog2/
	rm -f jumpshot.jar traceTOslog2.jar
	rm *.jar

force:
.PHONY: force clean jars
