This slog2sdk is a fork of the original slog2sdk, see:

https://www.mcs.anl.gov/research/projects/perfvis/download/index.htm#slog2sdk

Currently it's installed the binary version of slog2sdk and jumpshot. Future work is to provide a compilation of those with recent java compilers.
